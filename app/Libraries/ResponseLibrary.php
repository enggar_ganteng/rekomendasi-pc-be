<?php

namespace App\Libraries;

use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class ResponseLibrary {
  
	/**
     * @method format_response 
     * @param string $response_code
     * @param string $response_desc
     * @param string $action
     * @param string $response_data
	 * @return json $response
	 */
  	public function format_response($response_code, $response_desc, $action=null, $response_data=null) {
        $response = [
			"response_code" => $response_code,
            "response_desc" => $response_desc,
            "action" 		=> $action,
			"response_data" => $response_data
			
        ];
		return $response;
	}
}