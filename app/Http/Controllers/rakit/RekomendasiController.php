<?php

namespace App\Http\Controllers\rakit;

use App\Http\Controllers\Controller;
use App\Libraries\Constant;
use App\Libraries\RequestLibrary;
use App\Libraries\ResponseLibrary;
use App\Models\RekomendasiModel;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class RekomendasiController extends Controller
{
    public function __construct()
    {
        $this->response = new ResponseLibrary();
        $this->request_param = new RequestLibrary();
        $this->rekomendasiModel = new RekomendasiModel();
    }

    public function insert_pc(Request $request){
        $rules = [
            "processor"=> "required",
            "motherBoard"=> "required",
            "memory"=> "required",
            "casing"=> "required",
            "ssd"=> "required",
            "vga"=> "required",
            "mouse"=> "required",
            "keyboard"=> "required",
            "monitor"=> "required",
            "totalPrice"=> "required"
        ];

        $validator = Validator::make($request->input(Constant::REQUEST_DATA), $rules);
        if ($validator->fails()) {
            return $this->response->format_response(Constant::RC_PARAM_NOT_VALID, $validator->errors()->first(), "insert_pc");
        }

        $param = $this->request_param->get_param($request->input(Constant::REQUEST_DATA));

        $data = [
            "processor"=> $param->processor,
            "motherBoard"=> $param->motherBoard,
            "memory"=> $param->memory,
            "casing"=> $param->casing,
            "ssd"=> $param->ssd,
            "vga"=> $param->vga,
            "mouse"=> $param->mouse,
            "keyboard"=> $param->keyboard,
            "monitor"=> $param->monitor,
            "totalPrice"=> $param->totalPrice
        ];

        $insert_pc = $this->rekomendasiModel->insert_pc($data);

        if(!$insert_pc){
            return $this->response->format_response('99', 'gagal insert', "insert_pc");
        }

        return $this->response->format_response(Constant::RC_SUCCESS, Constant::DESC_SUCCESS, "insert_pc", $data);
    }

    public function getPC(){
        $get_pc = \DB::connection('main')->table('savedpc')->get();

        if($get_pc == []){
            return $this->response->format_response('99', 'Gagal', "get_pc");
        }

        return $this->response->format_response(Constant::RC_SUCCESS, Constant::DESC_SUCCESS, "get_pc", $get_pc);
    }
}
