<?php

namespace App\Http\Controllers\rakit;

use App\Http\Controllers\Controller;
use App\Libraries\Constant;
use App\Libraries\RequestLibrary;
use App\Libraries\ResponseLibrary;
use App\Models\UserModel;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller{
    public function __construct() {
        $this->response 	  = new ResponseLibrary();
        $this->request_param  = new RequestLibrary();
        $this->userModel = new UserModel();
    }

    public function login(Request $request){
        $rules = [
            "email"=> "required",
            "password"=> "required"
        ];

        $validator = Validator::make($request->input(Constant::REQUEST_DATA), $rules);
        if ($validator->fails()) {
            return $this->response->format_response(Constant::RC_PARAM_NOT_VALID, $validator->errors()->first(), "get_mahasiswa");
        }

        $param = $this->request_param->get_param($request->input(Constant::REQUEST_DATA));

        $get_login = $this->userModel->login($param->email,$param->password);

        if(!$get_login){
            return $this->response->format_response('99', 'Username atau password salah', "login");
        }

        return $this->response->format_response(Constant::RC_SUCCESS, Constant::DESC_SUCCESS, "login", $get_login);
    }

}
