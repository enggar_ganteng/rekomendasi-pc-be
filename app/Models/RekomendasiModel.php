<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class RekomendasiModel extends Model
{
    protected $table = 'savedpc';

    function insert_pc($data){
        $insert_pc = \DB::connection('main')->table('savedpc')->insert($data);

        if(!$insert_pc){
            return false;
        }

        return true;
    }
}
