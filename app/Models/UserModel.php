<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserModel extends Model{
    protected $table = 'user';

    function login ($email,$password){
        $get_login = \DB::connection('main')->table('user')->where('email', $email)->where('password', $password)->first();

        if (!$get_login){
            return false;
        }

        return true;

    }
}
