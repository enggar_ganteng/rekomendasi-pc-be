<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



$router->get('/', function () use ($router) {
    return "API RAKIT PC. <br> Service Build With <b>" . $router->app->version() . "</b><br> API Version V.1.1.0";
});

$router->group(['prefix' => 'api/','middleware' => ['request_format','cors']], function ($app) {
    $app->post('login', ['uses' =>'rakit\LoginController@login']);
    $app->post('insert_pc', ['uses' =>'rakit\RekomendasiController@insert_pc']);
});

$router->group(['prefix' => 'api/','middleware' => ['cors']], function ($app) {
    $app->get('getPC', ['uses' =>'rakit\RekomendasiController@getPC']);
});



